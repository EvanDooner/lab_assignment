labassignment
==============

## A Moodle Lab Assignment Plugin

author: [Evan Dooner](evanjdooner@gmail.com)

version: 1.0, 2013-09-15

for: Moodle 2.4.3+

licence: [GNU GPL v3 or later](http://www.gnu.org/copyleft/gpl.html)


### Overview

lab_assignment is a activity module plugin for the Moodle Course Management System. It allows teachers to offer several lab time slots as options to students. Additional options can be specified, allowing individual lab slots to have sign-up limits, and providing a number of ways to display the results to students, if that is desired.

Students can submit their answers in order of preference, or signal that none of the provided options suit them. Students may also leave a comment to provide additional information.

Teachers are able to view the number of students in each slot, and are provided with the tools to alter students' choices, swap students between time slots, or remove a student's answer completely. Students are notified of these changes by internal Moodle messages, and email, where applicable.

Teachers can export the results of a lab assignment in the following formats: PDF document, ODS and XLS spreadsheets, and plain text.

### Installation

This plugin installs in the usual fashion

1. Place the labassignment folder in the /moodle/mod directory of your Moodle installation
2. While logged in as admin, visit the 'Notifications' page in the 'Site administration' settings
3. Follow the on-screen prompts to install the module plugin

### File Structure

A description of the standard activity module files and their functions can be found in [the Moodle documentation](http://docs.moodle.org/dev/Activity_modules).

The following files and folders are specific to this plugin:

* /lib -This folder contains the external libraries used by labassignment

  * /lib/html2pdf - This folder contains the html2pdf library. This library is responsible for generation the PDF report document from the supplied HTML data.

### Customization

The specifications for the report formats are laid out in report.php. Any changes should be made here.

The contents of the alert emails are specified in /lang/en/labassignment.php. The relevent items are:
* $string['messagesavedbody'] - specifies the plaintext body of the message sent when a student is assigned an answer
* $string['messagesavedbodyhtml'] - specifies the HTML body of the message sent when a student is assigned an answer
* $string['messageupdatebody'] - specifies the plaintext body of the message sent when a student's answer is updated
* $string['messageupdatebodyhtml'] - specifies the HTML body of the message sent when a student's answer is updated
* $string['messagedeletedbody'] - specifies the plaintext body of the message sent when a student's answer is deleted
* $string['messagedeletedbodyhtml']  - specifies the HTML body of the message sent when a student's answer is deleted
