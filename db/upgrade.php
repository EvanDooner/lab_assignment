<?php

// This file keeps track of upgrades to
// the labassignment module
//
// Sometimes, changes between versions involve
// alterations to database structures and other
// major things that may break installations.
//
// The upgrade function in this file will attempt
// to perform all the necessary actions to upgrade
// your older installation to the current version.
//
// If there's something it cannot do itself, it
// will tell you what you need to do.
//
// The commands in here will all be database-neutral,
// using the methods of database_manager class
//
// Please do not forget to use upgrade_set_timeout()
// before any action that may take longer time to finish.
/**
 * Plugin update operations
 *
 * @package    mod_labassignment
 * @copyright  2012-2013 Evan Dooner
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
function xmldb_labassignment_upgrade($oldversion) {
	global $CFG, $DB;
	
	$dbman = $DB->get_manager ();
	
	// Moodle v2.2.0 release upgrade line
	// Put any upgrade step following this
	
	// Moodle v2.3.0 release upgrade line
	// Put any upgrade step following this
	
	return true;
}


