<?php
/**
 * Definition of log events
 *
 * @package    mod_labassignment
 * @category   log
 * @copyright  2012-2013 Evan Dooner <evanjdooner@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * 
 * Based on:
 * package    mod_labassignment
 * category   log
 * copyright  2010 Petr Skoda (http://skodak.org)
 * license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$logs = array(
    array('module'=>'labassignment', 'action'=>'view', 'mtable'=>'labassignment', 'field'=>'name'),
    array('module'=>'labassignment', 'action'=>'update', 'mtable'=>'labassignment', 'field'=>'name'),
    array('module'=>'labassignment', 'action'=>'add', 'mtable'=>'labassignment', 'field'=>'name'),
    array('module'=>'labassignment', 'action'=>'report', 'mtable'=>'labassignment', 'field'=>'name'),
    array('module'=>'labassignment', 'action'=>'choose', 'mtable'=>'labassignment', 'field'=>'name'),
    array('module'=>'labassignment', 'action'=>'choose again', 'mtable'=>'labassignment', 'field'=>'name'),
	array('module'=>'labassignment', 'action'=>'updatechoice', 'mtable'=>'labassignment', 'field'=>'name'),
);