<?php
/**
 * Plugin capabilities
 *
 * @package    mod_labassignment
 * @copyright  2012-2013
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * Based on:
 * package    mod_choice
 * copyright  2006 Martin Dougiamas
 * license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(

    'mod/labassignment:addinstance' => array(
        'riskbitmask' => RISK_XSS,

        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        ),
        'clonepermissionsfrom' => 'moodle/course:manageactivities'
    ),
		
	'mod/labassignment:view' => array(
	
		'captype' => 'read',
		'contextlevel' => CONTEXT_MODULE,
		'archetypes' => array(
			'student' => CAP_ALLOW,
			'teacher' => CAP_ALLOW,
			'editingteacher' => CAP_ALLOW,
			'manager' => CAP_ALLOW
		)
	),

    'mod/labassignment:choose' => array(

        'captype' => 'write',
        'contextlevel' => CONTEXT_MODULE,
        'archetypes' => array(
            'student' => CAP_ALLOW,
            'teacher' => CAP_ALLOW,
            'editingteacher' => CAP_ALLOW
        )
    ),

    'mod/labassignment:readresponses' => array(

        'captype' => 'read',
        'contextlevel' => CONTEXT_MODULE,
        'archetypes' => array(
            'teacher' => CAP_ALLOW,
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        )
    ),

    'mod/labassignment:deleteresponses' => array(

        'captype' => 'write',
        'contextlevel' => CONTEXT_MODULE,
        'archetypes' => array(
            'teacher' => CAP_ALLOW,
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        )
    ),
		
	'mod/labassignment:updateresponses' => array(
	
			'captype' => 'write',
			'contextlevel' => CONTEXT_MODULE,
			'archetypes' => array(
					'teacher' => CAP_ALLOW,
					'editingteacher' => CAP_ALLOW,
					'manager' => CAP_ALLOW
			)
	),

    'mod/labassignment:downloadresponses' => array(

        'captype' => 'read',
        'contextlevel' => CONTEXT_MODULE,
        'archetypes' => array(
            'teacher' => CAP_ALLOW,
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        )
    ),
		
	// Receive a notification message of when own answer is saved by teacher.
	'mod/labassignment:emailnotifysave' => array(
			'captype' => 'read',
			'contextlevel' => CONTEXT_MODULE,
			'archetypes' => array()
	),
		
	// Receive a notification message of when own answer is updated.
	'mod/labassignment:emailnotifyupdate' => array(
			'captype' => 'read',
			'contextlevel' => CONTEXT_MODULE,
			'archetypes' => array()
	),
	
	// Receive a notification message of when own answer is deleted.
	'mod/labassignment:emailnotifydeleted' => array(
			'captype' => 'read',
			'contextlevel' => CONTEXT_MODULE,
			'archetypes' => array()
	),
);


