<?php
/**
 * Plugin messaging capabilities
 *
 * @package    mod_labassignment
 * @copyright  2012-2013 Evan Dooner
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();
$messageproviders = array (
		// Notify user of answer chosen for them
		'save' => array (
				'capability'  => 'mod/labassignment:emailnotifysave'
		),
		// Notify user of answer update
		'update' => array (
				'capability'  => 'mod/labassignment:emailnotifyupdate'
		),
		// Confirm a student's quiz attempt
		'deleted' => array (
				'capability'  => 'mod/labassignment:emailnotifydeleted'
		)
);