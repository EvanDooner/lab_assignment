<?php

    require_once("../../config.php");
    require_once("lib.php");
    require_once($CFG->libdir . '/completionlib.php');

    $id         = required_param('id', PARAM_INT);                 // Course Module ID
    $action     = optional_param('action', '', PARAM_ALPHA);
    $attemptids = optional_param_array('attemptid', array(), PARAM_INT); // array of attempt ids for delete action

    $url = new moodle_url('/mod/labassignment/view.php', array('id'=>$id));
    if ($action !== '') {
        $url->param('action', $action);
    }
    $PAGE->set_url($url);

    if (! $cm = get_coursemodule_from_id('labassignment', $id)) {
        print_error('invalidcoursemodule');
    }

    if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }

    require_course_login($course, false, $cm);

    if (!$labassignment = labassignment_get_labassignment($cm->instance)) {
        print_error('invalidcoursemodule');
    }

    $strlabassignment = get_string('modulename', 'labassignment');
    $strlabassignments = get_string('modulenameplural', 'labassignment');

    if (!$context = get_context_instance(CONTEXT_MODULE, $cm->id)) {
        print_error('badcontext');
    }

    if ($action == 'dellabassignment' and confirm_sesskey() and is_enrolled($context, NULL, 'mod/labassignment:choose') and $labassignment->allowupdate) {
        if ($answer = $DB->get_record('labassignment_answers', array('labassignmentid' => $labassignment->id, 'userid' => $USER->id))) {
            $DB->delete_records('labassignment_answers', array('id' => $answer->id));

            // Update completion state
            $completion = new completion_info($course);
            if ($completion->is_enabled($cm) && $labassignment->completionsubmit) {
                $completion->update_state($cm, COMPLETION_INCOMPLETE);
            }
        }
    }

    $PAGE->set_title(format_string($labassignment->name));
    $PAGE->set_heading($course->fullname);

    // Mark viewed by user (if required)
    $completion = new completion_info($course);
    $completion->set_module_viewed($cm);

/// Submit any new data if there is any
    if (data_submitted() && is_enrolled($context, NULL, 'mod/labassignment:choose') && confirm_sesskey()) {
        $timenow = time();
        if (has_capability('mod/labassignment:deleteresponses', $context)) {
            if ($action == 'delete') { //some responses need to be deleted
                labassignment_delete_responses($attemptids, $labassignment, $cm, $course); //delete responses.
                redirect("view.php?id=$cm->id");
            }
        }
        $answerarray = array();
        $answercount = optional_param('answercount', '', PARAM_INT);
        $answer = 0;
        $allanswers = '';
        if ($answercount > 0) {
        	for ($i = 0; $i < $answercount; $i++) {
        		$answerarray[$i] = optional_param('answer'.$i, '', PARAM_INT);
        	}
        	$answer = $answerarray[0];
        	$allanswers = implode(',', $answerarray);
        }
        $usercomment = optional_param('usercomment', '', PARAM_TEXT);

        if (empty($answer)) {
            redirect("view.php?id=$cm->id", get_string('mustchooseone', 'labassignment'));
        } else if (labassignment_array_has_dupes($answerarray)) {
        	redirect("view.php?id=$cm->id", get_string('duplicateanswers', 'labassignment'));
        }	else {
            labassignment_user_submit_response($answer, $allanswers, $usercomment, $labassignment, $USER->id, $course, $cm);
        }
        echo $OUTPUT->header();
        echo $OUTPUT->notification(get_string('labassignmentsaved', 'labassignment'),'notifysuccess');
    } else {
        echo $OUTPUT->header();
    }


/// Display the labassignment and possibly results
    add_to_log($course->id, "labassignment", "view", "view.php?id=$cm->id", $labassignment->id, $cm->id);

    /// Check to see if groups are being used in this labassignment
    $groupmode = groups_get_activity_groupmode($cm);

    if ($groupmode) {
        groups_get_activity_group($cm, true);
        groups_print_activity_menu($cm, $CFG->wwwroot . '/mod/labassignment/view.php?id='.$id);
    }
    $allresponses = labassignment_get_response_data($labassignment, $cm, $groupmode);   // Big function, approx 6 SQL calls per user


    if (has_capability('mod/labassignment:readresponses', $context)) {
        labassignment_show_reportlink($allresponses, $cm);
    }

    echo '<div class="clearer"></div>';

    if ($labassignment->intro) {
        echo $OUTPUT->box(format_module_intro('labassignment', $labassignment, $cm->id), 'generalbox', 'intro');
    }

    $timenow = time();
    $current = false;  // Initialise for later
    //if user has already made a selection, and they are not allowed to update it or if labassignment is not open, show their selected answer.
    if (isloggedin() && ($current = $DB->get_record('labassignment_answers', array('labassignmentid' => $labassignment->id, 'userid' => $USER->id))) &&
        (empty($labassignment->allowupdate) || ($timenow > $labassignment->timeclose)) ) {
    	$choiceidlist = $current->alloptionsids;
    	$choiceids = explode(',', $choiceidlist);
    	$choicetext = '';
    	for ($i = 0; $i < count($choiceids); $i++) {
    		$choicenumber = $i + 1;
    		$choicetext .= 'Preference '.$choicenumber.' - ';
    		$choicetext .= format_string(labassignment_get_option_text($labassignment, $choiceids[$i]));
    		$choicetext .= ' | ';
    	}
    	$choicetext = rtrim($choicetext, ' | ');
        echo $OUTPUT->box(get_string("yourselection", "labassignment", userdate($labassignment->timeopen)).": ".$choicetext, 'generalbox', 'yourselection');
    }

/// Print the form
    $labassignmentopen = true;
    if ($labassignment->timeclose !=0) {
        if ($labassignment->timeopen > $timenow ) {
            echo $OUTPUT->box(get_string("notopenyet", "labassignment", userdate($labassignment->timeopen)), "generalbox notopenyet");
            echo $OUTPUT->footer();
            exit;
        } else if ($timenow > $labassignment->timeclose) {
            echo $OUTPUT->box(get_string("expired", "labassignment", userdate($labassignment->timeclose)), "generalbox expired");
            $labassignmentopen = false;
        }
    }

    if ( (!$current or $labassignment->allowupdate) and $labassignmentopen and is_enrolled($context, NULL, 'mod/labassignment:choose')) {
    // They haven't made their labassignment yet or updates allowed and labassignment is open

    	$default = labassignment_prepare_default_option($labassignment);
        $options = labassignment_prepare_options($labassignment, $USER, $cm, $allresponses);
        $renderer = $PAGE->get_renderer('mod_labassignment');
        echo $renderer->display_options($default, $options, $labassignment->commentguidelines, $cm->id, $labassignment->display);
        $labassignmentformshown = true;
    } else {
        $labassignmentformshown = false;
    }

    if (!$labassignmentformshown) {
        $sitecontext = get_context_instance(CONTEXT_SYSTEM);

        if (isguestuser()) {
            // Guest account
            echo $OUTPUT->confirm(get_string('noguestchoose', 'labassignment').'<br /><br />'.get_string('liketologin'),
                         get_login_url(), new moodle_url('/course/view.php', array('id'=>$course->id)));
        } else if (!is_enrolled($context)) {
            // Only people enrolled can make a labassignment
            $SESSION->wantsurl = qualified_me();
            $SESSION->enrolcancel = (!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '';

            $coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);
            $courseshortname = format_string($course->shortname, true, array('context' => $coursecontext));

            echo $OUTPUT->box_start('generalbox', 'notice');
            echo '<p align="center">'. get_string('notenrolledchoose', 'labassignment') .'</p>';
            echo $OUTPUT->container_start('continuebutton');
            echo $OUTPUT->single_button(new moodle_url('/enrol/index.php?', array('id'=>$course->id)), get_string('enrolme', 'core_enrol', $courseshortname));
            echo $OUTPUT->container_end();
            echo $OUTPUT->box_end();

        }
    }

    // print the results at the bottom of the screen
    if ( $labassignment->showresults == LABASSIGNMENT_SHOWRESULTS_ALWAYS or
        ($labassignment->showresults == LABASSIGNMENT_SHOWRESULTS_AFTER_ANSWER and $current) or
        ($labassignment->showresults == LABASSIGNMENT_SHOWRESULTS_AFTER_CLOSE and !$labassignmentopen)) {

        if (!empty($labassignment->showunanswered)) {
            $labassignment->option[0] = get_string('notanswered', 'labassignment');
            $labassignment->maxanswers[0] = 0;
        }
        $results = prepare_labassignment_show_results($labassignment, $course, $cm, $allresponses);
        $renderer = $PAGE->get_renderer('mod_labassignment');
        echo $renderer->display_result($results);

    } else if (!$labassignmentformshown) {
        echo $OUTPUT->box(get_string('noresultsviewable', 'labassignment'));
    }

    echo $OUTPUT->footer();
