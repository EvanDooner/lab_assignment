<?php
/**
 * Strings for component 'labassignment', language 'en', branch 'MOODLE_20_STABLE'
 * @package   labassignment
 * @copyright 2012-2013 Evan Dooner
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * Based on:
 * package   choice
 * copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addmorelabassignments'] = 'Add more lab choices';
$string['allowupdate'] = 'Allow lab choice to be updated';
$string['answered'] = 'Answered';
$string['atleastoneoption'] = 'You need to provide at least one possible answer.';
$string['commentguidelines'] = 'Comment Box Guidelines';
$string['commentguidelinesinfo'] = 'Add placeholder text to the comment box';
$string['commentguidelines_help'] = 'This text will appear in the comment box. Use it to inform students of the correct usage of the comment box';
$string['completionsubmit'] = 'Show as complete when user makes a lab choice';
$string['defaultoption'] = 'Default Option';
$string['defaultoption_help'] = 'Students can choose this answer to indicate that none of the other options suit them';
$string['displayhorizontal'] = 'Display horizontally';
$string['displaymode'] = 'Display mode';
$string['displayvertical'] = 'Display vertically';
$string['downloadpdf'] = 'Download in PDF format';
$string['duplicateanswers'] = 'You have made duplicate choices. Please choose a different option for each preference';
$string['expired'] = 'Sorry, this activity closed on {$a} and is no longer available';
$string['full'] = '(Full)';
$string['havetologin'] = 'You have to log in before you can submit your lab choice';
$string['labassignment'] = 'Lab Assignment';
$string['labassignment:addinstance'] = 'Add a new lab assignment';
$string['labassignmentclose'] = 'Until';
$string['labassignment:deleteresponses'] = 'Delete responses';
$string['labassignment:downloadresponses'] = 'Download responses';
$string['labassignmentfull'] = 'This lab slot is full and there are no available places.';
$string['labassignment:choose'] = 'Record a lab slot';
$string['labassignmentname'] = 'Lab Assignment name';
$string['labassignmentopen'] = 'Open';
$string['labassignmentoptions'] = 'Lab Assignment options';
$string['labassignmentoptions_help'] = 'Here is where you specify the options that participants have to choose from.

You can fill in any number of these. If you leave some of the options blank, they will not be displayed. If you need more than 8 options, click the "Add 3 fields to form" button.';
$string['limitanswers_help'] = 'This option allows you to limit the number of participants that can select each lab slot option. When the limit is reached then no-one else can select that option.

If limits are disabled then any number of participants can select each of the options.';
$string['labassignment:readresponses'] = 'Read responses';
$string['labassignmentsaved'] = 'Your lab choice has been saved';
$string['labassignmentslot'] = 'Lab Slot';
$string['labassignmenttext'] = 'Lab Assignment text';
$string['chooseaction'] = 'Choose an action ...';
$string['limit'] = 'Limit';
$string['limitanswers'] = 'Limit the number of responses allowed';
$string['messageprovider:save'] = 'Notification of an initial asnwer beassigned for your lab assignment choices';
$string['messageprovider:update'] = 'Notification of an update to your lab assignment choices';
$string['messageprovider:deleted'] = 'Notification of deletion of your lab assignment submissions';
$string['messagesavedbody'] = 'You have been assigned a slot in the lab assignment module {$a->modulename} in {$a->courseshortname} {$a->coursename}.
Please review your choices to ensure they are compatible with your timetable';
$string['messagesavedbodyhtml'] = 'You have been assigned a slot in the lab assignment module "{$a->modulename}" in {$a->courseshortname} {$a->coursename}.
<br />
Please review your choices to ensure they are compatible with your timetable
<br /><br />
{$a->fromname}';
$string['messageupdatebody'] = 'Your choices in the lab assignment module {$a->modulename} in {$a->courseshortname} {$a->coursename} have been updated.
Please review your choices to ensure they are correct';
$string['messageupdatebodyhtml'] = 'Your choices in the lab assignment module "{$a->modulename}" in {$a->courseshortname} {$a->coursename} have been updated.
<br />
Please review your choices to ensure they are correct
<br /><br />
{$a->fromname}';
$string['messagedeletedbody'] = 'Your choices in the lab assignment module {$a->modulename} in {$a->courseshortname} {$a->coursename} have been reset.
Please visit the module to make another choice';
$string['messagedeletedbodyhtml'] = 'Your choices in the lab assignment module "{$a->modulename}" in {$a->courseshortname} {$a->coursename} have been reset.
<br />
Please visit the module to make another choice
<br /><br />
{$a->fromname}';
$string['modulename'] = 'Lab Assignment';
$string['modulename_help'] = 'The lab assignment activity module enables a teacher to offer a selection of possible lab slots. Students may choose their top two preferences.

Lab Assignment results may be published after students have answered, after a certain date, or not at all. Results may be published with student names or anonymously.

A lab assignment activity may be used

* To allow students to select their prefered lab slot';
$string['modulename_link'] = 'mod/labassignment/view';
$string['modulenameplural'] = 'Lab Assignments';
$string['moveselectedusersto'] = 'Move selected users to...';
$string['mustchooseone'] = 'You must choose an answer before saving.  Nothing was saved.';
$string['noguestchoose'] = 'Sorry, guests are not allowed to make choices.';
$string['noresultsviewable'] = 'The results are not currently viewable.';
$string['notanswered'] = 'Not answered yet';
$string['notenrolledchoose'] = 'Sorry, only enrolled users are allowed to make choices.';
$string['notopenyet'] = 'Sorry, this activity is not available until {$a}';
$string['numberofuser'] = 'The number of user';
$string['numberofuserinpercentage'] = 'The number of user in percentage';
$string['option'] = 'Option';
$string['page-mod-labassignment-x'] = 'Any lab assignment module page';
$string['pluginadministration'] = 'Lab Assignment administration';
$string['pluginname'] = 'Lab Assignment';
$string['privacy'] = 'Privacy of results';
$string['publish'] = 'Publish results';
$string['publishafteranswer'] = 'Show results to students after they answer';
$string['publishafterclose'] = 'Show results to students only after the lab assignment is closed';
$string['publishalways'] = 'Always show results to students';
$string['publishanonymous'] = 'Publish anonymous results, do not show student names';
$string['publishnames'] = 'Publish full results, showing names and their lab choices';
$string['publishnot'] = 'Do not publish results to students';
$string['removemylabassignment'] = 'Remove my lab choice';
$string['removeresponses'] = 'Remove all responses';
$string['responses'] = 'Responses';
$string['responsesresultgraphheader'] = 'Graph display';
$string['responsesto'] = 'Responses to {$a}';
$string['savemylabassignment'] = 'Save my lab choices';
$string['showunanswered'] = 'Show column for unanswered';
$string['spaceleft'] = 'space available';
$string['spacesleft'] = 'spaces available';
$string['studentnumber'] = 'Student Number';
$string['taken'] = 'Taken';
$string['timerestrict'] = 'Restrict answering to this time period';
$string['viewallresponses'] = 'View {$a} responses';
$string['withselected'] = 'With selected';
$string['userchoosethisoption'] = 'User choose this option';
$string['yourselection'] = 'Your selection';
$string['skipresultgraph'] = 'Skip result graph';
