<?php

    require_once("../../config.php");
    require_once("lib.php");
    
    global $USER;

    $id         = required_param('id', PARAM_INT);   //moduleid
    $format     = optional_param('format', LABASSIGNMENT_PUBLISH_NAMES, PARAM_INT);
    $download   = optional_param('download', '', PARAM_ALPHA);
    $action     = optional_param('action', '', PARAM_ALPHA);
    $action2     = optional_param('action2', '', PARAM_ALPHA);
    $attemptids = optional_param_array('attemptid', array(), PARAM_INT); //get array of responses to delete.

    $url = new moodle_url('/mod/labassignment/report.php', array('id'=>$id));
    if ($format !== LABASSIGNMENT_PUBLISH_NAMES) {
        $url->param('format', $format);
    }
    if ($download !== '') {
        $url->param('download', $download);
    }
    if ($action !== '') {
        $url->param('action', $action);
    }
    if ($action2 !== '') {
    	$url->param('action2', $action2);
    }
    $PAGE->set_url($url);

    if (! $cm = get_coursemodule_from_id('labassignment', $id)) {
        print_error("invalidcoursemodule");
    }

    if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error("coursemisconf");
    }

    require_login($course, false, $cm);

    $context = get_context_instance(CONTEXT_MODULE, $cm->id);

    require_capability('mod/labassignment:readresponses', $context);

    if (!$labassignment = labassignment_get_labassignment($cm->instance)) {
        print_error('invalidcoursemodule');
    }

    $strlabassignment = get_string("modulename", "labassignment");
    $strlabassignments = get_string("modulenameplural", "labassignment");
    $strresponses = get_string("responses", "labassignment");

    add_to_log($course->id, "labassignment", "report", "report.php?id=$cm->id", "$labassignment->id",$cm->id);

    if (data_submitted() && $action == 'delete' && has_capability('mod/labassignment:deleteresponses',$context) && confirm_sesskey()) {
        labassignment_delete_responses($attemptids, $labassignment, $cm, $course); //delete responses.
        redirect("report.php?id=$cm->id");
    }
    
    if (data_submitted() && ($action == 'update' || $action2 == 'update') && has_capability('mod/labassignment:updateresponses',$context) && confirm_sesskey()) {
    	
    	$users = labassignment_get_response_data($labassignment, $cm, 0);
    	$options = labassignment_prepare_options($labassignment, $USER, $cm, $users);
    	$optioncount = count($options['options']);
    	
    	$answerstoupdate = array();
    	
    	foreach ($attemptids as $attemptid) {
    		foreach (range(0, $optioncount - 1) as $num) {
    			$answerstoupdate[$attemptid][$num] = optional_param('answer-'.$attemptid.'-'.$num, '0', PARAM_INT);
    		}
    		$answerstoupdate[$attemptid]['comment'] = optional_param('comment-'.$attemptid, '', PARAM_TEXT);
    	}
    	
    	labassignment_update_responses($answerstoupdate, $labassignment, $cm, $course); //update responses.
    	redirect("report.php?id=$cm->id");
    }

    if (!$download) {
        $PAGE->navbar->add($strresponses);
        $PAGE->set_title(format_string($labassignment->name).": $strresponses");
        $PAGE->set_heading($course->fullname);
        echo $OUTPUT->header();
        /// Check to see if groups are being used in this labassignment
        $groupmode = groups_get_activity_groupmode($cm);
        if ($groupmode) {
            groups_get_activity_group($cm, true);
            groups_print_activity_menu($cm, $CFG->wwwroot . '/mod/labassignment/report.php?id='.$id);
        }
    } else {
        $groupmode = groups_get_activity_groupmode($cm);
    }
    
    $users = labassignment_get_response_data($labassignment, $cm, $groupmode);
	
	// Print PDF file if one is asked for:
    if ($download == "pdf" && has_capability('mod/labassignment:downloadresponses', $context)) {
		require_once('/lib/html2pdf/html2pdf.class.php');
		
		$tablename = clean_filename("$course->shortname ".strip_tags(format_string($labassignment->name,true)));
        $filename = $tablename.'.pdf';

        /// Getting heading names
		$head = array();
		$head[] = get_string("studentnumber","labassignment");
        $head[] = get_string("firstname");
		$head[] = get_string("lastname");
        $head[] = get_string("group");
        $head[] = get_string("labassignmentslot","labassignment");

        /// generate the data for the body of the table
		$data = array();
        if ($users) {
            foreach ($users as $option => $userid) {
                $option_text = labassignment_get_option_text($labassignment, $option);
                foreach($userid as $user) {
					$line = array();
					$line['user'] = $user->idnumber;
					$line['fname'] = $user->firstname;
                    $line['lname'] = $user->lastname;
                    $ug2 = '';
                    if ($usergrps = groups_get_all_groups($course->id, $user->id)) {
                        foreach ($usergrps as $ug) {
                            $ug2 .= $ug->name;
                        }
                    }
                    $line['ug'] =  $ug2;
                    $line['ot'] = 'test';
                    if (isset($option_text)) {
                        $line['ot'] = format_string($option_text,true);
                    }
					$data[] = $line;
                }
				
            }
            
            $data = labassignment_sort_pdf_report_data($data, $labassignment, $users);
			$form = generate_table($data, $head, $tablename);
			$pdf = new HTML2PDF();
			$pdf->writeHTML($form);
			$pdf->Output($filename, 'D');
        }
		
        exit;
    }

	// Print ODS spreadsheet if one is asked for:
    if ($download == "ods" && has_capability('mod/labassignment:downloadresponses', $context)) {
        require_once("$CFG->libdir/odslib.class.php");

    /// Calculate file name
        $filename = clean_filename("$course->shortname ".strip_tags(format_string($labassignment->name,true))).'.ods';
    /// Creating a workbook
        $workbook = new MoodleODSWorkbook("-");
    /// Send HTTP headers
        $workbook->send($filename);
    /// Creating the first worksheet
        $myxls = $workbook->add_worksheet($strresponses);

    /// Print names of all the fields
        $myxls->write_string(0,0,get_string("studentnumber","labassignment"));
        $myxls->write_string(0,1,get_string("firstname"));
        $myxls->write_string(0,2,get_string("lastname"));
        $myxls->write_string(0,3,get_string("group"));
        $myxls->write_string(0,4,get_string("labassignment","labassignment"));

    /// generate the data for the body of the spreadsheet
        $row=1;
        if ($users) {
            foreach ($users as $option => $userid) {
                $option_text = labassignment_get_option_text($labassignment, $option);
                foreach($userid as $user) {

                	$myxls->write_string($row,0,$user->idnumber);
                    $myxls->write_string($row,1,$user->firstname);
                    $myxls->write_string($row,2,$user->lastname);
                    $ug2 = '';
                    if ($usergrps = groups_get_all_groups($course->id, $user->id)) {
                        foreach ($usergrps as $ug) {
                            $ug2 = $ug2. $ug->name;
                        }
                    }
                    $myxls->write_string($row,3,$ug2);

                    if (isset($option_text)) {
                        $myxls->write_string($row,4,format_string($option_text,true));
                    }
                    $row++;
                }
            }
        }
        /// Close the workbook
        $workbook->close();

        exit;
    }

    // Print Excel spreadsheet if one is asked for:
    if ($download == "xls" && has_capability('mod/labassignment:downloadresponses', $context)) {
        require_once("$CFG->libdir/excellib.class.php");

    /// Calculate file name
        $filename = clean_filename("$course->shortname ".strip_tags(format_string($labassignment->name,true))).'.xls';
    /// Creating a workbook
        $workbook = new MoodleExcelWorkbook("-");
    /// Send HTTP headers
        $workbook->send($filename);
    /// Creating the first worksheet
        $myxls = $workbook->add_worksheet($strresponses);

    /// Print names of all the fields
        $myxls->write_string(0,0,get_string("studentnumber","labassignment"));
        $myxls->write_string(0,1,get_string("firstname"));
        $myxls->write_string(0,2,get_string("lastname"));
        $myxls->write_string(0,3,get_string("group"));
        $myxls->write_string(0,4,get_string("labassignment","labassignment"));


    /// generate the data for the body of the spreadsheet
        $row=1;
        if ($users) {
            foreach ($users as $option => $userid) {
                $option_text = labassignment_get_option_text($labassignment, $option);
                foreach($userid as $user) {
                	$myxls->write_string($row,0,$user->idnumber);
                    $myxls->write_string($row,1,$user->firstname);
                    $myxls->write_string($row,2,$user->lastname);
                    $ug2 = '';
                    if ($usergrps = groups_get_all_groups($course->id, $user->id)) {
                        foreach ($usergrps as $ug) {
                            $ug2 = $ug2. $ug->name;
                        }
                    }
                    $myxls->write_string($row,3,$ug2);
                    if (isset($option_text)) {
                        $myxls->write_string($row,4,format_string($option_text,true));
                    }
                    $row++;
                }
            }
        }
        /// Close the workbook
        $workbook->close();
        exit;
    }

    // print text file if one is asked for:
    if ($download == "txt" && has_capability('mod/labassignment:downloadresponses', $context)) {
        $filename = clean_filename("$course->shortname ".strip_tags(format_string($labassignment->name,true))).'.txt';

        header("Content-Type: application/download\n");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Expires: 0");
        header("Cache-Control: must-revalidate,post-check=0,pre-check=0");
        header("Pragma: public");

        /// Print names of all the fields

        echo get_string("studentnumber","labassignment")."\t".get_string("firstname") . "\t". get_string("lastname") . "\t";
        echo get_string("group"). "\t";
        echo get_string("labassignment","labassignment"). "\n";

        /// generate the data for the body of the spreadsheet
        if ($users) {
            foreach ($users as $option => $userid) {
                $option_text = labassignment_get_option_text($labassignment, $option);
                foreach($userid as $user) {
                	
                	$result = $DB->get_record("user", array("id" => $user->id));
                	$studentid = $user->idnumber;
                	echo $studentid."\t";
                	echo $user->firstname."\t";
                    echo $user->lastname."\t";
                   
                    $ug2 = '';
                    if ($usergrps = groups_get_all_groups($course->id, $user->id)) {
                        foreach ($usergrps as $ug) {
                            $ug2 = $ug2. $ug->name;
                        }
                    }
                    echo $ug2. "\t";
                    if (isset($option_text)) {
                        echo format_string($option_text,true);
                    }
                    echo "\n";
                }
            }
        }
        exit;
    }

    $results = prepare_labassignment_show_results($labassignment, $course, $cm, $users, true);

   //now give links for downloading spreadsheets.
    if (!empty($users) && has_capability('mod/labassignment:downloadresponses',$context)) {
        $downloadoptions = array();
        $options = array();
        $options["id"] = "$cm->id";
		
		$options["download"] = "pdf";
        $button = $OUTPUT->single_button(new moodle_url("report.php", $options), get_string("downloadpdf", "labassignment"));
        $downloadoptions[] = html_writer::tag('li', $button, array('class'=>'reportoption'));
		
        $options["download"] = "ods";
        $button =  $OUTPUT->single_button(new moodle_url("report.php", $options), get_string("downloadods"));
        $downloadoptions[] = html_writer::tag('li', $button, array('class'=>'reportoption'));

        $options["download"] = "xls";
        $button = $OUTPUT->single_button(new moodle_url("report.php", $options), get_string("downloadexcel"));
        $downloadoptions[] = html_writer::tag('li', $button, array('class'=>'reportoption'));

        $options["download"] = "txt";
        $button = $OUTPUT->single_button(new moodle_url("report.php", $options), get_string("downloadtext"));
        $downloadoptions[] = html_writer::tag('li', $button, array('class'=>'reportoption'));

        $downloadlist = html_writer::tag('ul', implode('', $downloadoptions));
        $downloadlist .= html_writer::tag('div', '', array('class'=>'clearfloat'));
        echo html_writer::tag('div',$downloadlist, array('class'=>'downloadreport'));
    }
    echo $OUTPUT->footer();
    
    /**
     * Generates a HTML2PDF-compatible table from pre-formatted data
     *
     * @param array $data Prefomatted table data
     * @param array $head Preformatted table headings
     * @param string $title A title for the table
     * @return string $table A HTML table in string form
     */
    function generate_table($data, $head, $title = null){
    
    	//Format HTML table
    	$table = '<table style="width: 100%; border: solid 1px black; border-collapse: collapse">
		<thead>';
    
    	if(isset($title)){
    		$table .= '<tr>
			<th colspan="'.count($head).'" style="border:solid 1px black; padding:5px; text-align:center">'.htmlspecialchars($title).'</th>
				</tr>';
    	}
    	$table .= '<tr>';
    
    	foreach ($head as $h){
    		$table .= '<th style="border: solid 1px black; padding:5px; text-align:center">'.htmlspecialchars($h).'</th>';
    			
    	}
    	$table .=	'</tr>
		</thead>
    
		<tbody>';
    	foreach ($data as $row){
    		$table .= '<tr>';
    		foreach ($row as $col){
    			$table .= '<td style="border: solid 1px black; padding:5px">'.htmlspecialchars($col).'</td>';
    
    		}
    		$table .= '</tr>';
    			
    	}
    
    	$table .= '</tbody>
	</table>';
    	return $table;
    }

