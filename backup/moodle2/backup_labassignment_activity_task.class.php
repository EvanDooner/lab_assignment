<?php
/**
 * Defines backup_labassignment_activity_task class
 *
 * @package     mod_labassignment
 * @category    backup
 * @copyright   2012-2013 Evan Dooner <evanjdooner@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * 
 * Based on:
 * package     mod_choice
 * category    backup
 * copyright   2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/labassignment/backup/moodle2/backup_labassignment_stepslib.php');
require_once($CFG->dirroot . '/mod/labassignment/backup/moodle2/backup_labassignment_settingslib.php');

/**
 * Provides the steps to perform one complete backup of the Lab Assignment instance
 */
class backup_labassignment_activity_task extends backup_activity_task {

    /**
     * No specific settings for this activity
     */
    protected function define_my_settings() {
    }

    /**
     * Defines a backup step to store the instance data in the labassignment.xml file
     */
    protected function define_my_steps() {
        $this->add_step(new backup_labassignment_activity_structure_step('labassignment_structure', 'labassignment.xml'));
    }

    /**
     * Encodes URLs to the index.php and view.php scripts
     *
     * @param string $content some HTML text that eventually contains URLs to the activity instance scripts
     * @return string the content with the URLs encoded
     */
    static public function encode_content_links($content) {
        global $CFG;

        $base = preg_quote($CFG->wwwroot,"/");

        // Link to the list of labassignments
        $search="/(".$base."\/mod\/labassignment\/index.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@labASSIGNMENTINDEX*$2@$', $content);

        // Link to labassignment view by moduleid
        $search="/(".$base."\/mod\/labassignment\/view.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@labASSIGNMENTVIEWBYID*$2@$', $content);

        return $content;
    }
}
