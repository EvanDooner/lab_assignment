<?php
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once ($CFG->dirroot.'/course/moodleform_mod.php');

class mod_labassignment_mod_form extends moodleform_mod {

    function definition() {
        global $CFG, $LABASSIGNMENT_SHOWRESULTS, $LABASSIGNMENT_PUBLISH, $LABASSIGNMENT_DISPLAY, $DB;

        $mform    =& $this->_form;

//-------------------------------------------------------------------------------
        $mform->addElement('header', 'general', get_string('general', 'form')); //Adds header saysing "General"

        $mform->addElement('text', 'name', get_string('labassignmentname', 'labassignment'), array('size'=>'64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');

        $this->add_intro_editor(true, get_string('chatintro', 'chat')); //Adds introduction text box

//-------------------------------------------------------------------------------

        // Default Option
        $mform->addElement('header', '', get_string('defaultoption','labassignment'));
        $mform->addElement('text', 'defaultoption', get_string('defaultoption','labassignment'));
        $mform->addHelpButton('defaultoption', 'defaultoption', 'labassignment');
        $mform->addRule('defaultoption', null, 'required', null, 'client');
        
        // Limit Answers
        $menuoptions = array();
        $menuoptions[0] = get_string('disable');
        $menuoptions[1] = get_string('enable');
        $mform->addElement('header', 'timerestricthdr', get_string('limit', 'labassignment'));
        $mform->addElement('select', 'limitanswers', get_string('limitanswers', 'labassignment'), $menuoptions);
        $mform->addHelpButton('limitanswers', 'limitanswers', 'labassignment');
        
        // Standard Options
        $repeatarray = array();
        $repeatarray[] = $mform->createElement('header', '', get_string('option','labassignment').' {no}');
        $repeatarray[] = $mform->createElement('text', 'option', get_string('option','labassignment'));
        $repeatarray[] = $mform->createElement('text', 'limit', get_string('limit','labassignment'));
        $repeatarray[] = $mform->createElement('hidden', 'optionid', 0);

        if ($this->_instance){
            $repeatno = $DB->count_records('labassignment_options', array('labassignmentid'=>$this->_instance));
            $repeatno += 2;
        } else {
            $repeatno = 5;
        }

        $repeateloptions = array();
        $repeateloptions['limit']['default'] = 0;
        $repeateloptions['limit']['disabledif'] = array('limitanswers', 'eq', 0);
        $repeateloptions['limit']['rule'] = 'numeric';

        $repeateloptions['option']['helpbutton'] = array('labassignmentoptions', 'labassignment');
        $mform->setType('option', PARAM_CLEANHTML);

        $mform->setType('optionid', PARAM_INT);

        $this->repeat_elements($repeatarray, $repeatno,
                    $repeateloptions, 'option_repeats', 'option_add_fields', 3);

		// Comment Box Guidelines
        // Default Option
        $mform->addElement('header', '', get_string('commentguidelines','labassignment'));
        $mform->addElement('textarea', 'commentguidelines', get_string('commentguidelinesinfo','labassignment'), 'wrap="virtual" rows="10" cols="50"');
        $mform->addHelpButton('commentguidelines', 'commentguidelines', 'labassignment');


//-------------------------------------------------------------------------------
        $mform->addElement('header', 'timerestricthdr', get_string('timerestrict', 'labassignment'));
        $mform->addElement('checkbox', 'timerestrict', get_string('timerestrict', 'labassignment'));

        $mform->addElement('date_time_selector', 'timeopen', get_string("labassignmentopen", "labassignment"));
        $mform->disabledIf('timeopen', 'timerestrict');

        $mform->addElement('date_time_selector', 'timeclose', get_string("labassignmentclose", "labassignment"));
        $mform->disabledIf('timeclose', 'timerestrict');

//-------------------------------------------------------------------------------
        $mform->addElement('header', 'miscellaneoussettingshdr', get_string('miscellaneoussettings', 'form'));

        $mform->addElement('select', 'display', get_string("displaymode","labassignment"), $LABASSIGNMENT_DISPLAY);

        $mform->addElement('select', 'showresults', get_string("publish", "labassignment"), $LABASSIGNMENT_SHOWRESULTS);

        $mform->addElement('select', 'publish', get_string("privacy", "labassignment"), $LABASSIGNMENT_PUBLISH);
        $mform->disabledIf('publish', 'showresults', 'eq', 0);

        $mform->addElement('selectyesno', 'allowupdate', get_string("allowupdate", "labassignment"));

        $mform->addElement('selectyesno', 'showunanswered', get_string("showunanswered", "labassignment"));


//-------------------------------------------------------------------------------
        $this->standard_coursemodule_elements();
//-------------------------------------------------------------------------------
        $this->add_action_buttons();
    }

    function data_preprocessing(&$default_values) {
		global $DB;
		
		if (! empty ( $this->_instance )) {
			if (($labassignment = $DB->get_record ( 'labassignment', array('id' => $this->_instance) ))) {
				$default_values ['defaultoption'] = $labassignment->defaultoption;
				$default_values ['commentguidelines'] = $labassignment->commentguidelines;
			}
			
			if (($options = $DB->get_records_menu ( 'labassignment_options', array (
					'labassignmentid' => $this->_instance 
			), 'id', 'id,text' )) && ($options2 = $DB->get_records_menu ( 'labassignment_options', array (
					'labassignmentid' => $this->_instance 
			), 'id', 'id,maxanswers' ))) {
				$labassignmentids = array_keys ( $options );
				$options = array_values ( $options );
				$options2 = array_values ( $options2 );
				
				foreach ( array_keys ( $options ) as $key ) {
					$default_values ['option[' . $key . ']'] = $options [$key];
					$default_values ['limit[' . $key . ']'] = $options2 [$key];
					$default_values ['optionid[' . $key . ']'] = $labassignmentids [$key];
				}
			}
		}
		if (empty ( $default_values ['timeopen'] )) {
			$default_values ['timerestrict'] = 0;
		} else {
			$default_values ['timerestrict'] = 1;
		}
	}

    function validation($data, $files) {
        $errors = parent::validation($data, $files);

        $labassignments = 0;
        foreach ($data['option'] as $option){
            if (trim($option) != ''){
                $labassignments++;
            }
        }

        if ($labassignments < 1) {
           $errors['option[0]'] = get_string('atleastoneoption', 'labassignment');
        }

        return $errors;
    }

    function get_data() {
        $data = parent::get_data();
        if (!$data) {
            return false;
        }
        // Set up completion section even if checkbox is not ticked
        if (empty($data->completionsection)) {
            $data->completionsection=0;
        }
        return $data;
    }

    function add_completion_rules() {
        $mform =& $this->_form;

        $mform->addElement('checkbox', 'completionsubmit', '', get_string('completionsubmit', 'labassignment'));
        return array('completionsubmit');
    }

    function completion_rule_enabled($data) {
        return !empty($data['completionsubmit']);
    }
}

