<?php
/**
 * Version information
 *
 * @package    mod
 * @subpackage labassignment
 * @copyright  2012-2013 Evan Dooner <evanjdooner@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * 
 * Based on:
 * package    mod
 * subpackage course
 * copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$module->version   = 2013091500;    // The current module version (Date: YYYYMMDDXX)
$module->requires  = 2012120303;    // Requires this Moodle version
$module->component = 'mod_labassignment';     // Full name of the plugin (used for diagnostics)
$module->cron      = 0;
