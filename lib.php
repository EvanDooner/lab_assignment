<?php
/**
 * @package    mod_labassignment
 * @copyright  2012-2013
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * Based on:
 * package    mod_choice
 * copyright  2006 Martin Dougiamas
 * license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/** @global int $LABASSIGNMENT_COLUMN_HEIGHT */
global $LABASSIGNMENT_COLUMN_HEIGHT;
$LABASSIGNMENT_COLUMN_HEIGHT = 300;

/** @global int $LABASSIGNMENT_COLUMN_WIDTH */
global $LABASSIGNMENT_COLUMN_WIDTH;
$LABASSIGNMENT_COLUMN_WIDTH = 300;

define('LABASSIGNMENT_PUBLISH_ANONYMOUS', '0');
define('LABASSIGNMENT_PUBLISH_NAMES',     '1');

define('LABASSIGNMENT_SHOWRESULTS_NOT',          '0');
define('LABASSIGNMENT_SHOWRESULTS_AFTER_ANSWER', '1');
define('LABASSIGNMENT_SHOWRESULTS_AFTER_CLOSE',  '2');
define('LABASSIGNMENT_SHOWRESULTS_ALWAYS',       '3');

define('LABASSIGNMENT_DISPLAY_HORIZONTAL',  '0');
define('LABASSIGNMENT_DISPLAY_VERTICAL',    '1');

/** @global array $LABASSIGNMENT_PUBLISH */
global $LABASSIGNMENT_PUBLISH;
$LABASSIGNMENT_PUBLISH = array (LABASSIGNMENT_PUBLISH_ANONYMOUS  => get_string('publishanonymous', 'labassignment'),
                         LABASSIGNMENT_PUBLISH_NAMES      => get_string('publishnames', 'labassignment'));

/** @global array $LABASSIGNMENT_SHOWRESULTS */
global $LABASSIGNMENT_SHOWRESULTS;
$LABASSIGNMENT_SHOWRESULTS = array (LABASSIGNMENT_SHOWRESULTS_NOT          => get_string('publishnot', 'labassignment'),
                         LABASSIGNMENT_SHOWRESULTS_AFTER_ANSWER => get_string('publishafteranswer', 'labassignment'),
                         LABASSIGNMENT_SHOWRESULTS_AFTER_CLOSE  => get_string('publishafterclose', 'labassignment'),
                         LABASSIGNMENT_SHOWRESULTS_ALWAYS       => get_string('publishalways', 'labassignment'));

/** @global array $LABASSIGNMENT_DISPLAY */
global $LABASSIGNMENT_DISPLAY;
$LABASSIGNMENT_DISPLAY = array (LABASSIGNMENT_DISPLAY_HORIZONTAL   => get_string('displayhorizontal', 'labassignment'),
                         LABASSIGNMENT_DISPLAY_VERTICAL     => get_string('displayvertical','labassignment'));

define('LABASSIGNMENT_DEFAULT_VALUE', -1);

/// Standard functions /////////////////////////////////////////////////////////

/**
 * @global object
 * @param object $course
 * @param object $user
 * @param object $mod
 * @param object $labassignment
 * @return object|null
 */
function labassignment_user_outline($course, $user, $mod, $labassignment) {
    global $DB;
    if ($answer = $DB->get_record('labassignment_answers', array('labassignmentid' => $labassignment->id, 'userid' => $user->id))) {
        $result = new stdClass();
        $result->info = "'".format_string(labassignment_get_option_text($labassignment, $answer->optionid))."'";
        $result->time = $answer->timemodified;
        return $result;
    }
    return NULL;
}

/**
 * @global object
 * @param object $course
 * @param object $user
 * @param object $mod
 * @param object $labassignment
 * @return string|void
 */
function labassignment_user_complete($course, $user, $mod, $labassignment) {
    global $DB;
    if ($answer = $DB->get_record('labassignment_answers', array("labassignmentid" => $labassignment->id, "userid" => $user->id))) {
        $result = new stdClass();
        $result->info = "'".format_string(labassignment_get_option_text($labassignment, $answer->optionid))."'";
        $result->time = $answer->timemodified;
        echo get_string("answered", "labassignment").": $result->info. ".get_string("updated", '', userdate($result->time));
    } else {
        print_string("notanswered", "labassignment");
    }
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @global object
 * @param object $labassignment
 * @return int
 */
function labassignment_add_instance($labassignment) {
    global $DB;

    $labassignment->timemodified = time();

    if (empty($labassignment->timerestrict)) {
        $labassignment->timeopen = 0;
        $labassignment->timeclose = 0;
    }
		
		// insert answers
	$labassignment->id = $DB->insert_record ( "labassignment", $labassignment );
	
	foreach ( $labassignment->option as $key => $value ) {
		$value = trim ( $value );
		if (isset ( $value ) && $value != '') {
			$option = new stdClass ();
			$option->text = $value;
			$option->labassignmentid = $labassignment->id;
			if (isset ( $labassignment->limit [$key] )) {
				$option->maxanswers = $labassignment->limit [$key];
			}
			$option->timemodified = time ();
			$DB->insert_record ( "labassignment_options", $option );
		}
	}
	
	return $labassignment->id;
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @global object
 * @param object $labassignment
 * @return bool
 */
function labassignment_update_instance($labassignment) {
    global $DB;

    $labassignment->id = $labassignment->instance;
    $labassignment->timemodified = time();


    if (empty($labassignment->timerestrict)) {
        $labassignment->timeopen = 0;
        $labassignment->timeclose = 0;
    }
    
    // Other answers
    foreach ($labassignment->option as $key => $value) {
        $value = trim($value);
        $option = new stdClass();
        $option->text = $value;
        $option->labassignmentid = $labassignment->id;
        if (isset($labassignment->limit[$key])) {
            $option->maxanswers = $labassignment->limit[$key];
        }
        $option->timemodified = time();
        if (isset($labassignment->optionid[$key]) && !empty($labassignment->optionid[$key])){//existing labassignment record
            $option->id=$labassignment->optionid[$key];
            if (isset($value) && $value <> '') {
                $DB->update_record("labassignment_options", $option);
            } else { //empty old option - needs to be deleted.
                $DB->delete_records("labassignment_options", array("id"=>$option->id));
            }
        } else {
            if (isset($value) && $value <> '') {
                $DB->insert_record("labassignment_options", $option);
            }
        }
    }

    return $DB->update_record('labassignment', $labassignment);

}

/**
 * @global object
 * @param object $labassignment
 * @param object $coursemodule
 * @return array
 */
function labassignment_prepare_default_option($labassignment) {
	
	$text =  $labassignment->defaultoption;
	
	$defaultoption = new stdClass;
	if (isset($text)) { //make sure there are no dud entries in the db with blank text values.
		
		$defaultoption->attributes = new stdClass;
		$defaultoption->attributes->value = LABASSIGNMENT_DEFAULT_VALUE;
		$defaultoption->text = $text;
		$defaultoption->displaylayout = $labassignment->display;
	}
	
	return $defaultoption;
}

/**
 * @global object
 * @param object $labassignment
 * @param object $user
 * @param object $coursemodule
 * @param array $allresponses
 * @return array
 */
function labassignment_prepare_options($labassignment, $user, $coursemodule, $allresponses) {
    global $DB;

    $cdisplay = array('options'=>array());

    $cdisplay['limitanswers'] = true;
    $context = get_context_instance(CONTEXT_MODULE, $coursemodule->id);

    foreach ($labassignment->option as $optionid => $text) {
        if (isset($text)) { //make sure there are no dud entries in the db with blank text values.
            $option = new stdClass;
            $option->attributes = new stdClass;
            $option->attributes->value = $optionid;
            $option->text = $text;
            $option->maxanswers = $labassignment->maxanswers[$optionid];
            $option->displaylayout = $labassignment->display;

            if (isset($allresponses[$optionid])) {
                $option->countanswers = count($allresponses[$optionid]);
            } else {
                $option->countanswers = 0;
            }
            /*if ($DB->record_exists('labassignment_answers', array('labassignmentid' => $labassignment->id, 'userid' => $user->id, 'firstoptionid' => $firstoptionid, 'secondoptionid' => $secondoptionid))) {
                $option->attributes->checked = true;
            }*/
            if ( $labassignment->limitanswers && ($option->countanswers >= $option->maxanswers) && empty($option->attributes->checked)) {
                $option->attributes->disabled = true;
            }
            $cdisplay['options'][] = $option;
        }
    }

    $cdisplay['hascapability'] = is_enrolled($context, NULL, 'mod/labassignment:choose'); //only enrolled users are allowed to make a labassignment

    if ($labassignment->allowupdate && $DB->record_exists('labassignment_answers', array('labassignmentid'=> $labassignment->id, 'userid'=> $user->id))) {
        $cdisplay['allowupdate'] = true;
    }

    return $cdisplay;
}

/**
 * Submits a users answers
 * @param int $formanswer
 * @param array $allformanswers
 * @param string $usercomment
 * @param object $labassignment
 * @param int $userid
 * @param object $course
 * @param object $cm
 */
function labassignment_user_submit_response($formanswer, $allformanswers, $usercomment, $labassignment, $userid, $course, $cm) {
	global $DB, $CFG;
	require_once ($CFG->libdir . '/completionlib.php');
	
	$current = $DB->get_record ( 'labassignment_answers', array (
			'labassignmentid' => $labassignment->id,
			'userid' => $userid 
	) );
	$context = get_context_instance ( CONTEXT_MODULE, $cm->id );
	
	$countanswers = 0;
	if ($labassignment->limitanswers) {
		// Find out whether groups are being used and enabled
		if (groups_get_activity_groupmode ( $cm ) > 0) {
			$currentgroup = groups_get_activity_group ( $cm );
		} else {
			$currentgroup = 0;
		}
		if ($formanswer != - 1) {
			if ($currentgroup) {
				// If groups are being used, retrieve responses only for users in
				// current group
				global $CFG;
				$answers = $DB->get_records_sql ( "
SELECT
    ca.*
FROM
    {labassignment_answers} ca
    INNER JOIN {groups_members} gm ON ca.userid=gm.userid
WHERE
    optionid=?
    AND gm.groupid=?", array (
						$formanswer,
						$currentgroup 
				) );
			} else {
				// Groups are not used, retrieve all answers for this option ID
				$answers = $DB->get_records ( "labassignment_answers", array (
						"firstoptionid" => $formanswer 
				) );
			}
			
			if ($answers) {
				foreach ( $answers as $a ) { // only return enrolled users.
					if (is_enrolled ( $context, $a->userid, 'mod/labassignment:choose' )) {
						$countanswers ++;
					}
				}
			}
			$maxans = $labassignment->maxanswers [$formanswer];
		} else {
			$maxans = 100;
		}
	}
	
	if (! ($labassignment->limitanswers && ($countanswers >= $maxans))) {
		if ($current) {
			
			$newanswer = $current;
			$newanswer->firstoptionid = $formanswer;
			$newanswer->alloptionsids = $allformanswers;
			$newanswer->usercomment = $usercomment;
			$newanswer->canchooseagain = 0; // User can not resubmit choice
			$newanswer->timemodified = time ();
			$DB->update_record ( "labassignment_answers", $newanswer );
			add_to_log ( $course->id, "labassignment", "choose again", "view.php?id=$cm->id", $labassignment->id, $cm->id );
		} else {
			$newanswer = new stdClass ();
			$newanswer->labassignmentid = $labassignment->id;
			$newanswer->userid = $userid;
			$newanswer->firstoptionid = $formanswer;
			$newanswer->alloptionsids = $allformanswers;
			$newanswer->usercomment = $usercomment;
			$newanswer->canchooseagain = 0; // User can not resubmit choice
			$newanswer->timemodified = time ();
			$DB->insert_record ( "labassignment_answers", $newanswer );
			
			// Update completion state
			$completion = new completion_info ( $course );
			if ($completion->is_enabled ( $cm ) && $labassignment->completionsubmit) {
				$completion->update_state ( $cm, COMPLETION_COMPLETE );
			}
			add_to_log ( $course->id, "labassignment", "choose", "view.php?id=$cm->id", $labassignment->id, $cm->id );
		}
	} else {
		if (! ($current->optionid == $formanswer)) { // check to see if current labassignment already selected - if not display error
			print_error ( 'labassignmentfull', 'labassignment' );
		}
	}
}

/**
 * @param array $user
 * @param object $cm
 * @return void Output is echo'd
 */
function labassignment_show_reportlink($user, $cm) {
    $responsecount =0;
    foreach($user as $optionid => $userlist) {
        if ($optionid) {
            $responsecount += count($userlist);
        }
    }

    echo '<div class="reportlink">';
    echo "<a href=\"report.php?id=$cm->id\">".get_string("viewallresponses", "labassignment", $responsecount)."</a>";
    echo '</div>';
}

/**
 * Sorts the list of respondents in report.php in order of answer, with default answer last
 * @param array $respondents
 */
function labassignment_sort_respondents_for_report_view($respondents) {
	
	// Sort responses into arrays by response
	$sortingarray = array();
	foreach ($respondents as $userid => $user) {
		$sortingarray[$user->firstoption][] = $user;
	}
	
	// Remove default responses temporarily
	$defaultresponses = array();
	if(isset($sortingarray[LABASSIGNMENT_DEFAULT_VALUE])) {
		$defaultresponses = $sortingarray[LABASSIGNMENT_DEFAULT_VALUE];
		unset($sortingarray[LABASSIGNMENT_DEFAULT_VALUE]);
	}
	
	ksort($sortingarray);
	
	$sortingarray[] = $defaultresponses;
	
	$result = array();
	foreach ($sortingarray as $responsegroup) {
		usort($responsegroup, 'labassignment_sort_responses_comparator');
		foreach ($responsegroup as $response) {
			$result[] = $response;
		}
	}
	
	return $result;
}

function labassignment_sort_responses_comparator($a, $b) {
	
	if ($a->idnumber == $b->idnumber) {
		return 0;
	}
	return ($a->idnumber < $b->idnumber) ? -1 : 1;
	
}

/**
 * @param array $reportdata
 * @param object $labassignment
 * @param array $allresponses
 * @return array
 */
function labassignment_sort_pdf_report_data(array $reportdata, $labassignment, $allresponses) {
	
	$sortingarray = array();
	$optiontexts = array();
	
	foreach ($allresponses as $option => $userid) {
		$optiontexts[$option] = format_string(labassignment_get_option_text($labassignment, $option), true);
	}
	
	foreach ($reportdata as $line) {
		
		$optionid = array_search($line['ot'], $optiontexts);
		$sortingarray[$optionid][] = $line;
	}
	
	$defaultanswers = array();
	if (isset($sortingarray[LABASSIGNMENT_DEFAULT_VALUE])) {
		$defaultanswers = $sortingarray[LABASSIGNMENT_DEFAULT_VALUE];
		unset($sortingarray[LABASSIGNMENT_DEFAULT_VALUE]);
	}
	
	$unanswered = array();
	if (isset($sortingarray[0])) {
		$unanswered = $sortingarray[0];
		unset($sortingarray[0]);
	}
	
	ksort($sortingarray);
	
	$sortingarray[] = $defaultanswers;
	$sortingarray[] = $unanswered;
	
	$result = array();
	foreach ($sortingarray as $optionarray) {
		usort($optionarray, "labassignment_pdf_report_data_comparator");
		foreach ($optionarray as $line) {
			$result[] = $line;
		}
	}
	
	return $result;
	
}

/**
 * @param array $a
 * @param array $b
 * @return number
 */
function labassignment_pdf_report_data_comparator($a, $b) {
	
	if ($a['user'] == $b['user']) {
		return 0;
	}
	return ($a['user'] < $b['user']) ? -1 : 1;
	
}

/**
 * @param array $array
 * @return boolean true if array contains duplicate values; false otherwise
 */
function labassignment_array_has_dupes($array) {
	$dupe_array = array ();
	foreach ( $array as $val ) {
		if ($val != - 1) {
			if (++ $dupe_array [$val] > 1) {
				return true;
			}
		}
	}
	return false;
}

/**
 * @global object
 * @param object $labassignment
 * @param object $course
 * @param object $coursemodule
 * @param array $allresponses

 *  * @param bool $allresponses
 * @return object
 */
function prepare_labassignment_show_results($labassignment, $course, $cm, $allresponses, $forcepublish = false) {
	global $CFG, $LABASSIGNMENT_COLUMN_HEIGHT, $FULLSCRIPT, $PAGE, $OUTPUT, $DB, $USER;
	
	$display = clone ($labassignment);
	$display->coursemoduleid = $cm->id;
	$display->courseid = $course->id;
	
	// overwrite options value;
	$display->options = array ();
	$totaluser = 0;
	foreach ( $labassignment->option as $optionid => $optiontext ) {
		$display->options [$optionid] = new stdClass ();
		$display->options [$optionid]->text = $optiontext;
		$display->options [$optionid]->maxanswer = $labassignment->maxanswers [$optionid];
		
		if (array_key_exists ( $optionid, $allresponses )) {
			$display->options [$optionid]->user = $allresponses [$optionid];
			$totaluser += count ( $allresponses [$optionid] );
		}
	}
	unset ( $display->option );
	unset ( $display->maxanswers );
	
	$display->numberofuser = $totaluser;
	$context = get_context_instance ( CONTEXT_MODULE, $cm->id );
	$display->viewresponsecapability = has_capability ( 'mod/labassignment:readresponses', $context );
	$display->deleterepsonsecapability = has_capability ( 'mod/labassignment:deleteresponses', $context );
	$display->fullnamecapability = has_capability ( 'moodle/site:viewfullnames', $context );
	
	if (empty ( $allresponses )) {
		echo $OUTPUT->heading ( get_string ( "nousersyet" ) );
		return false;
	}
	
	$totalresponsecount = 0;
	foreach ( $allresponses as $optionid => $userlist ) {
		if ($labassignment->showunanswered || $optionid) {
			$totalresponsecount += count ( $userlist );
		}
	}
	
	$context = get_context_instance ( CONTEXT_MODULE, $cm->id );
	
	$hascapfullnames = has_capability ( 'moodle/site:viewfullnames', $context );
	
	$viewresponses = has_capability ( 'mod/labassignment:readresponses', $context );
	switch ($forcepublish) {
		case LABASSIGNMENT_PUBLISH_NAMES :
			echo '<div id="tablecontainer">';
			if ($viewresponses) {
				echo '<form id="attemptsform" method="post" action="' . $FULLSCRIPT . '" onsubmit="var menu = document.getElementById(\'menuaction\'); return (menu.options[menu.selectedIndex].value == \'delete\' ? \'' . addslashes_js ( get_string ( 'deleteattemptcheck', 'quiz' ) ) . '\' : true);">';
				echo '<div>';
				echo '<input type="hidden" name="id" value="' . $cm->id . '" />';
				echo '<input type="hidden" name="sesskey" value="' . sesskey () . '" />';
				echo '<input type="hidden" name="mode" value="overview" />';
			}
			
			echo "<table cellpadding=\"5\" cellspacing=\"10\" class=\"results names\">";
			echo "<tr>";
			
			$columncount = array (); // number of votes in each column
			$columncount [0] = 0;
			echo "<th class=\"col0 header\" scope=\"col\">";
			print_string ( 'notanswered', 'labassignment' );
			echo "</th>";
			
			$count = 1;
			foreach ( $labassignment->option as $optionid => $optiontext ) {
				if (isset ( $allresponses [$optionid] )) {
					$columncount [$optionid] = count ( $allresponses [$optionid] );
				} else {
					$columncount [$optionid] = 0;
				}
				echo "<th class=\"col$count header\" scope=\"col\">";
				echo format_string ( $optiontext );
				echo "</th>";
				$count ++;
			}
			// Add default answer column
			if (isset ( $allresponses [LABASSIGNMENT_DEFAULT_VALUE] )) {
				$columncount [LABASSIGNMENT_DEFAULT_VALUE] = count ( $allresponses [LABASSIGNMENT_DEFAULT_VALUE] );
			} else {
				$columncount [LABASSIGNMENT_DEFAULT_VALUE] = 0;
			}
			echo "<th class=\"col$count header\" scope=\"col\">";
			echo get_string('defaultoption', 'labassignment');
			echo "</th>";
			echo "</tr><tr>";
			
			$count = 1;
			
			echo "<td align=\"center\" class=\"col0 count\">";
			echo count ( $allresponses [0] );
			echo "</td>";
			
			foreach ( $labassignment->option as $optionid => $optiontext ) {
				echo "<td align=\"center\" class=\"col$count count\">";
				if ($labassignment->limitanswers) {
					echo get_string ( "taken", "labassignment" ) . ":";
					echo $columncount [$optionid];
					echo "<br/>";
					echo get_string ( "limit", "labassignment" ) . ":";
					echo $labassignment->maxanswers [$optionid];
				} else {
					if (isset ( $columncount [$optionid] )) {
						echo $columncount [$optionid];
					}
				}
				echo "</td>";
				$count ++;
			}
			// Add default answer numbers
			echo "<td align=\"center\" class=\"col$count count\">";
			if (isset ( $columncount [- 1] )) {
				echo $columncount [- 1];
			}
			echo "</td></tr>";
			
			// Users who have responded
			$groupmode = groups_get_activity_groupmode ( $cm );
			$respondents = labassignment_get_response_data_by_user ( $labassignment, $cm, $groupmode );
			$defaultoption = labassignment_prepare_default_option ( $labassignment, $cm );
			$options = labassignment_prepare_options ( $labassignment, $USER, $cm, $allresponses );
			$optioncount = count ( $options ['options'] );
			
			$selectoptions = array ();
			foreach ( $options ['options'] as $option ) {
				$labeltext = $option->text;
				if (! empty ( $option->attributes->disabled )) {
					$labeltext .= ' ' . get_string ( 'full', 'labassignment' );
				}
				$selectoptions [$option->attributes->value] = $labeltext;
			}
			$selectoptions [$defaultoption->attributes->value] = $defaultoption->text;
			
			$renderer = $PAGE->get_renderer ( 'mod_labassignment' );
			
			if (! empty ( $respondents )) {
				echo '</table><br />';
				echo "<table cellpadding=\"5\" cellspacing=\"10\" class=\"results names\" id=\"respondents\">";

				echo "<colgroup>
						<col span=\"1\" style=\"width: 15%;\">
						<col span=\"1\" style=\"width: 35%;\">
						<col span=\"1\" style=\"width: 50%;\">
					</colgroup>";
				
				echo "<tr><th class=\"user header\" scope=\"col\" colspan=\"3\">Responses</th></tr>";
				
				echo "<tr><th class=\"user header\" scope=\"col\">User</th>";
				echo "<th class=\"prefs header\" scope=\"col\">Preferences</th>";
				echo "<th class=\"comment header\" scope=\"col\">Comments</th>";
				echo "</tr>";
				
				if (! empty ( $respondents )) {
					
					$respondents = labassignment_sort_respondents_for_report_view($respondents);
					
					foreach ( $respondents as $user ) {
						
						// User info cell
						$data = '';
						if (empty ( $user->imagealt )) {
							$user->imagealt = '';
						}
						$userfullname = fullname ( $user, $display->fullnamecapability );
						$hascapabilities = $display->viewresponsecapability && $display->deleterepsonsecapability;
						if ($hascapabilities) {
							$attemptaction = html_writer::label ( $userfullname, 'attempt-user' . $user->id, false, array (
									'class' => 'accesshide' 
							) );
							$attemptaction .= html_writer::checkbox ( 'attemptid[]', $user->id, '', null, array (
									'id' => 'attempt-user' . $user->id 
							) );
							$data .= html_writer::tag ( 'div', $attemptaction, array (
									'class' => 'attemptaction' 
							) );
						}
						$userimage = $renderer->get_user_picture ( $user, array (
								'courseid' => $display->courseid 
						) );
						$data .= html_writer::tag ( 'div', $userimage, array (
								'class' => 'image' 
						) );
						
						$userlink = new moodle_url ( '/user/view.php', array (
								'id' => $user->id,
								'course' => $display->courseid 
						) );
						$name = html_writer::tag ( 'a', $userfullname, array (
								'href' => $userlink,
								'class' => 'username' 
						) );
						$data .= html_writer::tag ( 'div', $name, array (
								'class' => 'fullname' 
						) );
						$data .= html_writer::tag ( 'div', '', array (
								'class' => 'clearfloat' 
						) );
						$userinfo = html_writer::tag ( 'div', $data, array (
								'class' => 'user' 
						) );
						$usercell = html_writer::tag ( 'td', $userinfo, array (
								'class' => 'usercell' 
						) );
						$rowcontents = $usercell;
						
						// Preference cells
						$preferences = explode ( ',', $user->alloptions );
						$numprefs = count ( $options ['options'] );
						$currentoption = 0;
						$selecttablecontents = '';
						foreach ( range ( 0, $numprefs - 1 ) as $prefnum ) {
							
							$select = html_writer::select ( $selectoptions, 'answer-' . $user->id . '-' . $currentoption, $preferences [$prefnum], 'Choose...', array (
									'style' => 'width: 100%;' 
							) );
							$selectrowcontents = html_writer::tag ( 'td', $select, array (
									'style' => 'text-align: centre; width: 100%;' 
							) );
							$selectrow = html_writer::tag ( 'tr', $selectrowcontents, array (
									'id' => $user->id,
									'style' => 'text-align: centre; width: 100%;' 
							) );
							$selecttablecontents .= $selectrow;
							
							$currentoption ++;
						}
						$selecttablebody = html_writer::tag ( 'tbody', $selecttablecontents, array (
								'style' => 'text-align: centre; width: 100%;' 
						) );
						$selecttable = html_writer::tag ( 'table', $selecttablebody, array (
								'style' => 'text-align: centre; width: 100%;' 
						) );
						$selecttablecell = html_writer::tag ( 'td', $selecttable, array (
								'style' => 'text-align: centre;' 
						) );
						$rowcontents .= $selecttablecell;
						
						$usercommentbox = html_writer::tag ( 'textarea', $user->usercomment, array (
								'name' => 'comment-' . $user->id,
								'style' => 'width: 100%;',
								'wrap' => 'virtual',
								'rows' => '8' 
						) );
						
						$rowcontents .= '<td>' . $usercommentbox . '</td>';
						
						$row = html_writer::tag ( 'tr', $rowcontents, array (
								'id' => $user->id 
						) );
						echo $row;
					}
				}
				
				if ($viewresponses and has_capability ( 'mod/labassignment:deleteresponses', $context )) {
					echo '<tr><td></td><td>';
					echo '<a href="javascript:select_all_in(\'TABLE\',null,\'respondents\');">' . get_string ( 'selectall' ) . '</a> / ';
					echo '<a href="javascript:deselect_all_in(\'TABLE\',null,\'respondents\');">' . get_string ( 'deselectall' ) . '</a> ';
					echo '&nbsp;&nbsp;';
					echo html_writer::label ( get_string ( 'withselected', 'labassignment' ), 'menuaction' );
					echo html_writer::select ( array (
							'delete' => get_string ( 'delete' ),
							'update' => get_string ( 'update' ) 
					), 'action', '', array (
							'' => get_string ( 'withselectedusers' ) 
					), array (
							'id' => 'menuaction',
							'class' => 'autosubmit' 
					) );
					// $PAGE->requires->js_init_call('M.util.init_select_autosubmit', array('attemptsform', 'menuaction', ''));
					$PAGE->requires->yui_module ( 'moodle-core-formautosubmit', 'M.core.init_formautosubmit', array (
							array (
									'selectid' => 'menuaction',
									'nothing' => false 
							) 
					) );
					echo '<noscript id="noscriptmenuaction" style="display:inline">';
					echo '<div>';
					echo '<input type="submit" value="' . get_string ( 'go' ) . '" /></div></noscript>';
					echo '</td><td></td></tr>';
				}
			}
				
			// UNANSWERED USERS
			$respondents = $allresponses [0];
			$options = labassignment_prepare_options ( $labassignment, $USER, $cm, $allresponses );
			$optioncount = count ( $options ['options'] );
			
			if (count ( $respondents ) > 0) {
				
				echo "</table><br />";
				echo "<table cellpadding=\"5\" cellspacing=\"10\" class=\"results names\" id=\"nonrespondents\">";
				
				echo "<colgroup>
						<col span=\"1\" style=\"width: 15%;\">
						<col span=\"1\" style=\"width: 35%;\">
						<col span=\"1\" style=\"width: 50%;\">
					</colgroup>";
				
				echo "<tr><th class=\"user header\" scope=\"col\" colspan=\"3\">Users Who Have Yet To Respond</th></tr>";
				
				echo "<tr><th class=\"user header\" scope=\"col\">User</th>";
				echo "<th class=\"prefs header\" scope=\"col\">Preferences</th>";
				echo "<th class=\"comment header\" scope=\"col\">Comments</th>";
				echo "</tr>";
				
				if (! empty ( $respondents )) {
					foreach ( $respondents as $user ) {
						
						// User info cell
						$data = '';
						if (empty ( $user->imagealt )) {
							$user->imagealt = '';
						}
						$userfullname = fullname ( $user, $display->fullnamecapability );
						$hascapabilities = $display->viewresponsecapability && $display->deleterepsonsecapability;
						if ($hascapabilities) {
							$attemptaction = html_writer::label ( $userfullname, 'attempt-user' . $user->id, false, array (
									'class' => 'accesshide' 
							) );
							$attemptaction .= html_writer::checkbox ( 'attemptid[]', $user->id, '', null, array (
									'id' => 'attempt-user' . $user->id 
							) );
							$data .= html_writer::tag ( 'div', $attemptaction, array (
									'class' => 'attemptaction' 
							) );
						}
						$userimage = $renderer->get_user_picture ( $user, array (
								'courseid' => $display->courseid 
						) );
						$data .= html_writer::tag ( 'div', $userimage, array (
								'class' => 'image' 
						) );
						
						$userlink = new moodle_url ( '/user/view.php', array (
								'id' => $user->id,
								'course' => $display->courseid 
						) );
						$name = html_writer::tag ( 'a', $userfullname, array (
								'href' => $userlink,
								'class' => 'username' 
						) );
						$data .= html_writer::tag ( 'div', $name, array (
								'class' => 'fullname' 
						) );
						$data .= html_writer::tag ( 'div', '', array (
								'class' => 'clearfloat' 
						) );
						$userinfo = html_writer::tag ( 'div', $data, array (
								'class' => 'user' 
						) );
						$usercell = html_writer::tag ( 'td', $userinfo, array (
								'class' => 'usercell' 
						) );
						$rowcontents = $usercell;
						
						// Preference cells
						$numprefs = count ( $options ['options'] );
						$currentoption = 0;
						$selecttablecontents = '';
						foreach ( range ( 0, $numprefs - 1 ) as $prefnum ) {
							
							$select = html_writer::select ( $selectoptions, 'answer-' . $user->id . '-' . $currentoption, '', 'Choose...', array (
									'style' => 'width: 100%;' 
							) );
							$selectrowcontents = html_writer::tag ( 'td', $select, array (
									'style' => 'text-align: centre; width: 100%;' 
							) );
							$selectrow = html_writer::tag ( 'tr', $selectrowcontents, array (
									'id' => $user->id,
									'style' => 'text-align: centre; width: 100%;' 
							) );
							$selecttablecontents .= $selectrow;
							
							$currentoption ++;
						}
						$selecttablebody = html_writer::tag ( 'tbody', $selecttablecontents, array (
								'style' => 'text-align: centre; width: 100%;' 
						) );
						$selecttable = html_writer::tag ( 'table', $selecttablebody, array (
								'style' => 'text-align: centre; width: 100%;' 
						) );
						$selecttablecell = html_writer::tag ( 'td', $selecttable, array (
								'style' => 'text-align: centre;' 
						) );
						$rowcontents .= $selecttablecell;
						
						$usercommentbox = html_writer::tag ( 'textarea', '', array (
								'name' => 'comment-' . $user->id,
								'style' => 'width: 100%;',
								'wrap' => 'virtual',
								'rows' => '8' 
						) );
						
						$rowcontents .= '<td>' . $usercommentbox . '</td>';
						
						$row = html_writer::tag ( 'tr', $rowcontents, array (
								'id' => $user->id 
						) );
						echo $row;
					}
				}
				
				// / Print "Select all" etc.
				if ($viewresponses and has_capability ( 'mod/labassignment:deleteresponses', $context )) {
					echo '<tr><td></td><td>';
					echo '<a href="javascript:select_all_in(\'TABLE\',null,\'nonrespondents\');">' . get_string ( 'selectall' ) . '</a> / ';
					echo '<a href="javascript:deselect_all_in(\'TABLE\',null,\'nonrespondents\');">' . get_string ( 'deselectall' ) . '</a> ';
					echo '&nbsp;&nbsp;';
					echo html_writer::label ( get_string ( 'withselected', 'labassignment' ), 'menuaction' );
					echo html_writer::select ( array (
							'update' => get_string ( 'update' ) 
					), 'action2', '', array (
							'' => get_string ( 'withselectedusers' ) 
					), array (
							'id' => 'menuaction2',
							'class' => 'autosubmit' 
					) );
					// $PAGE->requires->js_init_call('M.util.init_select_autosubmit', array('attemptsform', 'menuaction', ''));
					$PAGE->requires->yui_module ( 'moodle-core-formautosubmit', 'M.core.init_formautosubmit', array (
							array (
									'selectid' => 'menuaction2',
									'nothing' => false 
							) 
					) );
					echo '<noscript id="noscriptmenuaction" style="display:inline">';
					echo '<div>';
					echo '<input type="submit" value="' . get_string ( 'go' ) . '" /></div></noscript>';
					echo '</td><td></td></tr>';
				}
			}
			
			echo "</table></div>";
			if ($viewresponses) {
				echo "</form></div>";
			}
			
			break;
	}
	return $display;
}

/**
 * @global object
 * @param array $attemptids
 * @param object $labassignment Lab_Assignment main table row
 * @param object $cm Course-module object
 * @param object $course Course object
 * @return bool
 */
function labassignment_delete_responses($attemptids, $labassignment, $cm, $course) {
    global $DB, $CFG, $USER;
    require_once($CFG->libdir.'/completionlib.php');
    
    $context = get_context_instance(CONTEXT_MODULE, $cm->id);
    $fullnamecapability = has_capability('moodle/site:viewfullnames', $context);

    if(!is_array($attemptids) || empty($attemptids)) {
        return false;
    }

    foreach($attemptids as $num => $attemptid) {
        if(empty($attemptid)) {
            unset($attemptids[$num]);
        }
    }

    $completion = new completion_info($course);
    foreach($attemptids as $attemptid) {
        if ($todelete = $DB->get_record('labassignment_answers', array('labassignmentid' => $labassignment->id, 'userid' => $attemptid))) {
            $DB->delete_records('labassignment_answers', array('labassignmentid' => $labassignment->id, 'userid' => $attemptid));
            // Update completion state
            if ($completion->is_enabled($cm) && $labassignment->completionsubmit) {
                $completion->update_state($cm, COMPLETION_INCOMPLETE, $attemptid);
            }
            // Send a message notifying the user of the deletion
            $eventdata = new stdClass();
            $eventdata->component         = 'mod_labassignment'; //your component name
            $eventdata->name              = 'deleted'; //this is the message name from messages.php
            $eventdata->userfrom          = $USER;
            $eventdata->userto            = $attemptid;
            $eventdata->subject           = 'Your choices for "'.$labassignment->name.'" ['.$course->shortname.'] have been reset';
            $messagevariables = new stdClass();
            $messagevariables->modulename = $labassignment->name;
            $messagevariables->courseshortname = $course->shortname;
            $messagevariables->coursename = $course->fullname;
            $messagevariables->fromname = fullname($USER, $fullnamecapability);
            $eventdata->fullmessage       = get_string("messagedeletedbody", "labassignment", $messagevariables);
            $eventdata->fullmessageformat = FORMAT_PLAIN;
            $eventdata->fullmessagehtml   = get_string("messagedeletedbodyhtml", "labassignment", $messagevariables);
            $eventdata->smallmessage      = get_string("messagedeletedbody", "labassignment", $messagevariables);
            $eventdata->notification      = 0; //this is only set to 0 for personal messages between users
            message_send($eventdata);
        }
    }
    return true;
}

/**
 * @global object
 * @param array $attemptids
 * @param object $labassignment Lab_Assignment main table row
 * @param object $cm Course-module object
 * @param object $course Course object
 * @return bool
 */
function labassignment_update_responses($attemptids, $labassignment, $cm, $course) {
	global $DB, $CFG, $USER;
	require_once($CFG->libdir.'/completionlib.php');
	
	$context = get_context_instance(CONTEXT_MODULE, $cm->id);
	$fullnamecapability = has_capability('moodle/site:viewfullnames', $context);

	if(!is_array($attemptids) || empty($attemptids)) {
		return false;
	}

	foreach($attemptids as $attemptid => $answers) {
		if(empty($answers)) {
			unset($attemptids[$attemptid]);
		}
	}
	
	foreach($attemptids as $attemptid => $answers) {
		
		$usercomment = $answers['comment'];
		unset($answers['comment']);
		$formanswer = $answers[0];
		$allformanswers = implode(',', $answers);
		
		if ($current = $DB->get_record('labassignment_answers', array('labassignmentid' => $labassignment->id, 'userid' => $attemptid))) {
			
			$previousoptions = $current->alloptionsids;
			
			$newanswer = $current;
            $newanswer->firstoptionid = $formanswer;
            $newanswer->alloptionsids = $allformanswers;
            $newanswer->usercomment = $usercomment;
            $newanswer->canchooseagain = 0; // User can not resubmit choice
            $newanswer->timemodified = time();
            $DB->update_record("labassignment_answers", $newanswer);
            add_to_log($course->id, "labassignment", "updatechoice", "view.php?id=$cm->id", $labassignment->id, $cm->id);
            
            // Only send update message if options have changed
            if ($previousoptions != $newanswer->alloptionsids) {
	            // Send a message notifying the user of the update
	            $eventdata = new stdClass();
	            $eventdata->component         = 'mod_labassignment'; //your component name
	            $eventdata->name              = 'update'; //this is the message name from messages.php
	            $eventdata->userfrom          = $USER;
	            $eventdata->userto            = $attemptid;
	            $eventdata->subject           = 'Your choices for "'.$labassignment->name.'" ['.$course->shortname.'] have been updated';
	            $messagevariables = new stdClass();
	            $messagevariables->modulename = $labassignment->name;
	            $messagevariables->courseshortname = $course->shortname;
	            $messagevariables->coursename = $course->fullname;
	            $messagevariables->fromname = fullname($USER, $fullnamecapability);
	            $eventdata->fullmessage       = get_string("messageupdatebody", "labassignment", $messagevariables);
	            $eventdata->fullmessageformat = FORMAT_PLAIN;
	            $eventdata->fullmessagehtml   = get_string("messageupdatebodyhtml", "labassignment", $messagevariables);
	            $eventdata->smallmessage      = get_string("messageupdatebody", "labassignment", $messagevariables);
	            $eventdata->notification      = 0; //this is only set to 0 for personal messages between users
	            message_send($eventdata);
            }
		} else {
			
			$newanswer = new stdClass();
			$newanswer->labassignmentid = $labassignment->id;
			$newanswer->userid = $attemptid;
			$newanswer->firstoptionid = $formanswer;
			$newanswer->alloptionsids = $allformanswers;
			$newanswer->usercomment = $usercomment;
			$newanswer->canchooseagain = 0; // User can not resubmit choice
			$newanswer->timemodified = time();
			$DB->insert_record("labassignment_answers", $newanswer);
			
			// Update completion state
			$completion = new completion_info($course);
			if ($completion->is_enabled($cm) && $labassignment->completionsubmit) {
				$completion->update_state($cm, COMPLETION_COMPLETE);
			}
			add_to_log($course->id, "labassignment", "choose", "view.php?id=$cm->id", $labassignment->id, $cm->id);
            
            // Send a message notifying the user of the update
            $eventdata = new stdClass();
            $eventdata->component         = 'mod_labassignment'; //your component name
            $eventdata->name              = 'save'; //this is the message name from messages.php
            $eventdata->userfrom          = $USER;
            $eventdata->userto            = $attemptid;
            $eventdata->subject           = 'You have been assigned an answer for "'.$labassignment->name.'" ['.$course->shortname.']';
            $messagevariables = new stdClass();
            $messagevariables->modulename = $labassignment->name;
            $messagevariables->courseshortname = $course->shortname;
            $messagevariables->coursename = $course->fullname;
            $messagevariables->fromname = fullname($USER, $fullnamecapability);
            $eventdata->fullmessage       = get_string("messagesavedbody", "labassignment", $messagevariables);
            $eventdata->fullmessageformat = FORMAT_PLAIN;
            $eventdata->fullmessagehtml   = get_string("messagesavedbodyhtml", "labassignment", $messagevariables);
            $eventdata->smallmessage      = get_string("messagesavedbody", "labassignment", $messagevariables);
            $eventdata->notification      = 0; //this is only set to 0 for personal messages between users
            message_send($eventdata);
		}
	}
	return true;
}


/**
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @global object
 * @param int $id
 * @return bool
 */
function labassignment_delete_instance($id) {
    global $DB;

    if (! $labassignment = $DB->get_record("labassignment", array("id"=>"$id"))) {
        return false;
    }

    $result = true;

    if (! $DB->delete_records("labassignment_answers", array("labassignmentid"=>"$labassignment->id"))) {
        $result = false;
    }

    if (! $DB->delete_records("labassignment_options", array("labassignmentid"=>"$labassignment->id"))) {
        $result = false;
    }

    if (! $DB->delete_records("labassignment", array("id"=>"$labassignment->id"))) {
        $result = false;
    }

    return $result;
}

/**
 * Returns text string which is the answer that matches the id
 *
 * @global object
 * @param object $labassignment
 * @param int $id
 * @return string
 */
function labassignment_get_option_text($labassignment, $id) {
    global $DB;

    if ($id == LABASSIGNMENT_DEFAULT_VALUE) {
    	return $labassignment->defaultoption;
    } else if ($result = $DB->get_record("labassignment_options", array("id" => $id))) {
        return $result->text;
    } else {
        return get_string("notanswered", "labassignment");
    }
}

/**
 * Gets a full labassignment record
 *
 * @global object
 * @param int $labassignmentid
 * @return object|bool The labassignment or false
 */
function labassignment_get_labassignment($labassignmentid) {
    global $DB;

    if ($labassignment = $DB->get_record("labassignment", array("id" => $labassignmentid))) {
    		
    		if ($options = $DB->get_records("labassignment_options", array("labassignmentid" => $labassignmentid), "id")) {
    			foreach ($options as $option) {
    				$labassignment->option[$option->id] = $option->text;
    				$labassignment->maxanswers[$option->id] = $option->maxanswers;
    			}
    			return $labassignment;
    		}
    }
    return false;
}

/**
 * @return array
 */
function labassignment_get_view_actions() {
    return array('view','view all','report');
}

/**
 * @return array
 */
function labassignment_get_post_actions() {
    return array('choose','choose again');
}


/**
 * Implementation of the function for printing the form elements that control
 * whether the course reset functionality affects the labassignment.
 *
 * @param object $mform form passed by reference
 */
function labassignment_reset_course_form_definition(&$mform) {
    $mform->addElement('header', 'labassignmentheader', get_string('modulenameplural', 'labassignment'));
    $mform->addElement('advcheckbox', 'reset_labassignment', get_string('removeresponses','labassignment'));
}

/**
 * Course reset form defaults.
 *
 * @return array
 */
function labassignment_reset_course_form_defaults($course) {
    return array('reset_labassignment'=>1);
}

/**
 * Actual implementation of the reset course functionality, delete all the
 * labassignment responses for course $data->courseid.
 *
 * @global object
 * @global object
 * @param object $data the data submitted from the reset course.
 * @return array status array
 */
function labassignment_reset_userdata($data) {
    global $CFG, $DB;

    $componentstr = get_string('modulenameplural', 'labassignment');
    $status = array();

    if (!empty($data->reset_labassignment)) {
        $labassignmentssql = "SELECT ch.id
                       FROM {labassignment} ch
                       WHERE ch.course=?";

        $DB->delete_records_select('labassignment_answers', "labassignmentid IN ($labassignmentssql)", array($data->courseid));
        $status[] = array('component'=>$componentstr, 'item'=>get_string('removeresponses', 'labassignment'), 'error'=>false);
    }

    /// updating dates - shift may be negative too
    if ($data->timeshift) {
        shift_course_mod_dates('labassignment', array('timeopen', 'timeclose'), $data->timeshift, $data->courseid);
        $status[] = array('component'=>$componentstr, 'item'=>get_string('datechanged'), 'error'=>false);
    }

    return $status;
}

/**
 * @global object
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @param object $labassignment
 * @param object $cm
 * @param int $groupmode
 * @return array
 */
function labassignment_get_response_data($labassignment, $cm, $groupmode) {
    global $CFG, $USER, $DB;

    $context = get_context_instance(CONTEXT_MODULE, $cm->id);

/// Get the current group
    if ($groupmode > 0) {
        $currentgroup = groups_get_activity_group($cm);
    } else {
        $currentgroup = 0;
    }

/// Initialise the returned array, which is a matrix:  $allresponses[responseid][userid] = responseobject
    $allresponses = array();

/// First get all the users who have access here
/// To start with we assume they are all "unanswered" then move them later
    $allresponses[0] = get_enrolled_users($context, 'mod/labassignment:choose', $currentgroup, user_picture::fields('u', array('idnumber')), 'u.lastname ASC,u.firstname ASC');

/// Get all the recorded responses for this labassignment
    $rawresponses = $DB->get_records('labassignment_answers', array('labassignmentid' => $labassignment->id));

/// Use the responses to move users into the correct column

    if ($rawresponses) {
        foreach ($rawresponses as $response) {
            if (isset($allresponses[0][$response->userid])) {   // This person is enrolled and in correct group
                $allresponses[0][$response->userid]->timemodified = $response->timemodified;
                $allresponses[$response->firstoptionid][$response->userid] = clone($allresponses[0][$response->userid]);
                unset($allresponses[0][$response->userid]);   // Remove from unanswered column
            }
        }
    }
    return $allresponses;
}

/**
 * @global object
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @param object $labassignment
 * @param object $cm
 * @param int $groupmode
 * @return array
 */
function labassignment_get_response_data_by_user($labassignment, $cm, $groupmode) {
	global $CFG, $USER, $DB;

	$context = get_context_instance(CONTEXT_MODULE, $cm->id);

	/// Get the current group
	if ($groupmode > 0) {
		$currentgroup = groups_get_activity_group($cm);
	} else {
		$currentgroup = 0;
	}

	/// Initialise the returned array
	$allresponses = array();

	/// First get all the users who have access here
	$allenrolledusers = get_enrolled_users($context, 'mod/labassignment:choose', $currentgroup, user_picture::fields('u', array('idnumber')), 'u.lastname ASC,u.firstname ASC');

	/// Get all the recorded responses for this labassignment
	$rawresponses = $DB->get_records('labassignment_answers', array('labassignmentid' => $labassignment->id));

	/// If a user has made a response, add their response to options to their user info and add them to the returned array
	if ($rawresponses) {
		foreach ($rawresponses as $response) {
			if (isset($allenrolledusers[$response->userid])) {   // This person is enrolled and in correct group and has made a response
				$allenrolledusers[$response->userid]->timemodified = $response->timemodified;
				$allenrolledusers[$response->userid]->firstoption = $response->firstoptionid;
				$allenrolledusers[$response->userid]->alloptions = $response->alloptionsids;
				$allenrolledusers[$response->userid]->usercomment = $response->usercomment;
				$allresponses[$response->userid] = clone($allenrolledusers[$response->userid]);
			}
		}
	}
	return $allresponses;
}

/**
 * Returns all other caps used in module
 *
 * @return array
 */
function labassignment_get_extra_capabilities() {
    return array('moodle/site:accessallgroups');
}

/**
 * @uses FEATURE_GROUPS
 * @uses FEATURE_GROUPINGS
 * @uses FEATURE_GROUPMEMBERSONLY
 * @uses FEATURE_MOD_INTRO
 * @uses FEATURE_COMPLETION_TRACKS_VIEWS
 * @uses FEATURE_GRADE_HAS_GRADE
 * @uses FEATURE_GRADE_OUTCOMES
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed True if module supports feature, null if doesn't know
 */
function labassignment_supports($feature) {
    switch($feature) {
        case FEATURE_GROUPS:                  return true;
        case FEATURE_GROUPINGS:               return true;
        case FEATURE_GROUPMEMBERSONLY:        return true;
        case FEATURE_MOD_INTRO:               return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return true;
        case FEATURE_COMPLETION_HAS_RULES:    return true;
        case FEATURE_GRADE_HAS_GRADE:         return false;
        case FEATURE_GRADE_OUTCOMES:          return false;
        case FEATURE_BACKUP_MOODLE2:          return true;
        case FEATURE_SHOW_DESCRIPTION:        return true;

        default: return null;
    }
}

/**
 * Adds module specific settings to the settings block
 *
 * @param settings_navigation $settings The settings navigation object
 * @param navigation_node $labassignmentnode The node to add module settings to
 */
function labassignment_extend_settings_navigation(settings_navigation $settings, navigation_node $labassignmentnode) {
    global $PAGE;

    if (has_capability('mod/labassignment:readresponses', $PAGE->cm->context)) {

        $groupmode = groups_get_activity_groupmode($PAGE->cm);
        if ($groupmode) {
            groups_get_activity_group($PAGE->cm, true);
        }
        // We only actually need the labassignment id here
        $labassignment = new stdClass;
        $labassignment->id = $PAGE->cm->instance;
        $allresponses = labassignment_get_response_data($labassignment, $PAGE->cm, $groupmode);   // Big function, approx 6 SQL calls per user

        $responsecount =0;
        foreach($allresponses as $optionid => $userlist) {
            if ($optionid) {
                $responsecount += count($userlist);
            }
        }
        $labassignmentnode->add(get_string("viewallresponses", "labassignment", $responsecount), new moodle_url('/mod/labassignment/report.php', array('id'=>$PAGE->cm->id)));
    }
}

/**
 * Obtains the automatic completion state for this labassignment based on any conditions
 * in forum settings.
 *
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not, $type if conditions not set.
 */
function labassignment_get_completion_state($course, $cm, $userid, $type) {
    global $CFG,$DB;

    // Get labassignment details
    $labassignment = $DB->get_record('labassignment', array('id'=>$cm->instance), '*',
            MUST_EXIST);

    // If completion option is enabled, evaluate it and return true/false
    if($labassignment->completionsubmit) {
        return $DB->record_exists('labassignment_answers', array(
                'labassignmentid'=>$labassignment->id, 'userid'=>$userid));
    } else {
        // Completion option is not enabled so just return $type
        return $type;
    }
}

/**
 * Return a list of page types
 * @param string $pagetype current page type
 * @param stdClass $parentcontext Block's parent context
 * @param stdClass $currentcontext Current context of block
 */
function labassignment_page_type_list($pagetype, $parentcontext, $currentcontext) {
    $module_pagetype = array('mod-labassignment-*'=>get_string('page-mod-labassignment-x', 'labassignment'));
    return $module_pagetype;
}
