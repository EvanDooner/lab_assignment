<?php

    require_once("../../config.php");
    require_once("lib.php");

    $id = required_param('id',PARAM_INT);   // course

    $PAGE->set_url('/mod/labassignment/index.php', array('id'=>$id));

    if (!$course = $DB->get_record('course', array('id'=>$id))) {
        print_error('invalidcourseid');
    }

    require_course_login($course);
    $PAGE->set_pagelayout('incourse');

    add_to_log($course->id, "labassignment", "view all", "index.php?id=$course->id", "");

    $strlabassignment = get_string("modulename", "labassignment");
    $strlabassignments = get_string("modulenameplural", "labassignment");
    $strsectionname  = get_string('sectionname', 'format_'.$course->format);
    $PAGE->set_title($strlabassignments);
    $PAGE->set_heading($course->fullname);
    $PAGE->navbar->add($strlabassignments);
    echo $OUTPUT->header();

    if (! $labassignments = get_all_instances_in_course("labassignment", $course)) {
        notice(get_string('thereareno', 'moodle', $strlabassignments), "../../course/view.php?id=$course->id");
    }

    $usesections = course_format_uses_sections($course->format);

    $sql = "SELECT cha.*
              FROM {labassignment} ch, {labassignment_answers} cha
             WHERE cha.labassignmentid = ch.id AND
                   ch.course = ? AND cha.userid = ?";

    $answers = array () ;
    if (isloggedin() and !isguestuser() and $allanswers = $DB->get_records_sql($sql, array($course->id, $USER->id))) {
        foreach ($allanswers as $aa) {
            $answers[$aa->labassignmentid] = $aa;
        }
        unset($allanswers);
    }


    $timenow = time();

    $table = new html_table();

    if ($usesections) {
        $table->head  = array ($strsectionname, get_string("question"), get_string("answer"));
        $table->align = array ("center", "left", "left");
    } else {
        $table->head  = array (get_string("question"), get_string("answer"));
        $table->align = array ("left", "left");
    }

    $currentsection = "";

    foreach ($labassignments as $labassignment) {
        if (!empty($answers[$labassignment->id])) {
            $answer = $answers[$labassignment->id];
        } else {
            $answer = "";
        }
        if (!empty($answer->optionid)) {
            $aa = format_string(labassignment_get_option_text($labassignment, $answer->optionid));
        } else {
            $aa = "";
        }
        if ($usesections) {
            $printsection = "";
            if ($labassignment->section !== $currentsection) {
                if ($labassignment->section) {
                    $printsection = get_section_name($course, $labassignment->section);
                }
                if ($currentsection !== "") {
                    $table->data[] = 'hr';
                }
                $currentsection = $labassignment->section;
            }
        }

        //Calculate the href
        if (!$labassignment->visible) {
            //Show dimmed if the mod is hidden
            $tt_href = "<a class=\"dimmed\" href=\"view.php?id=$labassignment->coursemodule\">".format_string($labassignment->name,true)."</a>";
        } else {
            //Show normal if the mod is visible
            $tt_href = "<a href=\"view.php?id=$labassignment->coursemodule\">".format_string($labassignment->name,true)."</a>";
        }
        if ($usesections) {
            $table->data[] = array ($printsection, $tt_href, $aa);
        } else {
            $table->data[] = array ($tt_href, $aa);
        }
    }
    echo "<br />";
    echo html_writer::table($table);

    echo $OUTPUT->footer();


